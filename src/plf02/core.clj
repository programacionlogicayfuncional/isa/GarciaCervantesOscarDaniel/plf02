(ns plf02.core)
;Predicados:
(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

;Ejemplos:
(función-associative?-1 [1 2 3])
(función-associative?-2 '(1 2 3))
(función-associative?-3 {:a 1 :b 2})

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

;Ejemplos:
(función-boolean?-1 false)
(función-boolean?-2 nil)
(función-boolean?-3 true)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

;Ejemplos:
(función-char?-1 \a)
(función-char?-2 22)
(función-char?-3 "a")

(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

;Ejemplos:
(función-coll?-1 [1 2 3])
(función-coll?-2 4)
(función-coll?-3 {:a 1 :b 2})

(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

;Ejemplos:
(función-decimal?-1 1)
(función-decimal?-2 1.0)
(función-decimal?-3 1M)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

;Ejemplos:
(función-double?-1 1)
(función-double?-2 1.0)
(función-double?-3 1.0M)

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

;Ejemplos:
(función-float?-1 0)
(función-float?-2 0.0)
(función-float?-3 1M)

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

;Ejemplos:
(función-ident?-1 :hola)
(función-ident?-2 'abc)
(función-ident?-3 "hola")

(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

;Ejemplos:
(función-indexed?-1 [1 2 3])
(función-indexed?-2 #{1 2 3})
(función-indexed?-3 {:a 1 :b 2})

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

;Ejemplos:
(función-int?-1 1)
(función-int?-2 1.0)
(función-int?-3 1N)

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

;Ejemplos:
(función-integer?-1 1)
(función-integer?-2 1.0)
(función-integer?-3 1N)

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

;Ejemplos:
(función-keyword?-1 :+)
(función-keyword?-2 true)
(función-keyword?-3 '+)

(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

;Ejemplos:
(función-list?-1 0)
(función-list?-2 '(1 2 3))
(función-list?-3 [1 2 3])

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

;Ejemplos:
(función-map-entry?-1 {:a 1 :b 2})
(función-map-entry?-2 (first {:a 1 :b 2}))
(función-map-entry?-3 nil)

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

;Ejemplos:
(función-map?-1 {:a 1 :b 2 :c 3})
(función-map?-2 '(1 2 3))
(función-map?-3 #{:a :b :c})

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

;Ejemplos:
(función-nat-int?-1 0)
(función-nat-int?-2 1)
(función-nat-int?-3 -1)

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

;Ejemplos:
(función-number?-1 1.0)
(función-number?-2 1)
(función-number?-3 :a)

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

;Ejemplos:
(función-pos-int?-1 0)
(función-pos-int?-2 1)
(función-pos-int?-3 -1)

(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

;Ejemplos:
(función-ratio?-1 22/7)
(función-ratio?-2 22)
(función-ratio?-3 2.2)

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

;Ejemplos:
(función-rational?-1 1)
(función-rational?-2 1.0)
(función-rational?-3 22/7)

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

;Ejemplos:
(función-seq?-1 '(1 2 3))
(función-seq?-2 #{1 2 3})
(función-seq?-3 [1 2 3])

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

;Ejemplos:
(función-seqable?-1 1)
(función-seqable?-2 nil)
(función-seqable?-3 [1 2])

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

;Ejemplos:
(función-sequential?-1 '(1 2 3))
(función-sequential?-2 [1 2 3])
(función-sequential?-3 "abc")

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

;Ejemplos:
(función-set?-1 #{1 2 3})
(función-set?-2 [1 2 3])
(función-set?-3 {:a 1 :b 2})

(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

;Ejemplos:
(función-some?-1 nil)
(función-some?-2 1)
(función-some?-3 false)

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

;Ejemplos:
(función-string?-1 "abc")
(función-string?-2 \a)
(función-string?-3 ["a" "b" "c"])

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

;Ejemplos:
(función-symbol?-1 'a)
(función-symbol?-2 1)
(función-symbol?-3 :a)

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

;Ejemplos:
(función-vector?-1 [1 2 3])
(función-vector?-2 '(1 2 3))
(función-vector?-3 {:a 1 :b 2 :c 3})

;Funciones de orden superior:

(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

;Ejemplos
(función-drop-1 1 [1 2 3])
(función-drop-2 2 '(1 2 3))
(función-drop-3 2 {:a 1 :b 2 :c 3 :d 4})

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

;Ejemplos
(función-drop-last-1 [1 2 3])
(función-drop-last-2 2 '(1 2 3 4))
(función-drop-last-3 2 {:a 1 :b 2 :c 3 :d 4})

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

;Ejemplos
(función-drop-while-1 neg? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-2 float? [-1 -2 -6 -7 3 2.2 3 4 -5 -6 0 1])
(función-drop-while-3 integer? [1 2 3 4])

(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

;Ejemplos
(función-every?-1 even? '(2 4 6))
(función-every?-2 string? '())
(función-every?-3 map? '())

(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

;Ejemplos
(función-filterv-1 even? (range 10))
(función-filterv-2 true? [true false true false true])
(función-filterv-3 false? [true false true false true])

(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

;Ejemplos
(función-group-by-1 count ["a" "as" "asd" "aa" "asdf" "qwer"])
(función-group-by-2 odd? (range 10))
(función-group-by-3 true? [true false true false true])

(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

;Ejemplos
(función-iterate-1 inc 5)
(función-iterate-2 dec 4)
(take 4 (función-iterate-3 (partial + 2) 0))

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

;Ejemplos
(función-keep-1 even? (range 1 10))
(función-keep-2 true? [true 2 false 4])
(función-keep-3 false? [true 2 false 4])

(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

;Ejemplos
(función-keep-indexed-1 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-2 #(if (pos? %2) %1) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 #(when (odd? %1) %2) [2 5 3 4 6 7 9 8])

(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

;Ejemplos
(función-map-indexed-1 vector "hola")
(función-map-indexed-2 hash-map "hola")
(función-map-indexed-3 list [:a :b :c])

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [a b c]
  (mapcat a b c))

;Ejemplos
(función-mapcat-1 reverse [[3 2 1 0] [1 2 3]])
(función-mapcat-2 #(repeat 2 %) [1 2])
(función-mapcat-3 list [:a :b :c] [1 2 3])

(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b c]
  (mapv a b c))

(defn función-mapv-3
  [a b c]
  (mapv a b c))

;Ejemplos
(función-mapv-1 inc [1 2 3 4 5])
(función-mapv-2 + [1 2 3] [4 5 6])
(función-mapv-3 + [1 2 3] (iterate inc 1))

(defn función-merge-with-1
  [a b c]
  (merge-with a b c))

(defn función-merge-with-2
  [a b c d]
  (merge-with a b c d))

(defn función-merge-with-3
  [a b c d]
  (merge-with a b c d))

;Ejemplos
(función-merge-with-1 + {:a 1  :b 2} {:a 9  :b 98 :c 0})
(función-merge-with-2 + {:a 1  :b 2} {:a 9  :b 98  :c 0} {:a 10 :b 100 :c 10})
(función-merge-with-3 + {:a 1} {:a 2} {:a 3})

(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

;Ejemplos
(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 nil? [true false false])
(función-not-any?-3 odd? '(1 2 3))

(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

;Ejemplos
(función-not-every?-1 odd? '(1 2 3))
(función-not-every?-2 odd? '(1 3))
(función-not-every?-3 nil? [true false false])

(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

;Ejemplos
(función-partition-by-1 #(= 3 %) [1 2 3 4 5])
(función-partition-by-2 odd? [1 1 1 2 2 3 3])
(función-partition-by-3 even? [1 1 1 2 2 3 3])

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

;Ejemplos
(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 1 :b 2 :c 3})
(función-reduce-kv-2 #(assoc %1 %3 %2) {:a 1} {:a 1 :b 2 :c 3})
(función-reduce-kv-3 #(assoc %1 %3 %2) {:d 4 :e 4 :f 5} {:a 1 :b 2 :c 3})

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

;Ejemplos
(función-remove-1 pos? [1 -2 2 -1 3 7 0])
(función-remove-2 nil? [1 nil 2 nil 3 nil])
(función-remove-3 even? (range 10))

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

;Ejemplos
(función-reverse-1 "clojure")
(función-reverse-2 '(1 2 3))
(función-reverse-3 [1 2 3 4])

(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

;Ejemplos
(función-some-1 true? [false true false])
(función-some-2 even? '(1 2 3 4))
(función-some-3 {2 "two" 3 "three"} [nil 3 2])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b c]
  (sort-by a b c))

;Ejemplos
(función-sort-by-1 count ["aaa" "bb" "c"])
(función-sort-by-2 first [[1 2] [2 2] [2 3]])
(función-sort-by-3 first > [[1 2] [2 2] [2 3]])

(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

;Ejemplos
(función-split-with-1 (partial >= 3) [1 2 3 4 5])
(función-split-with-2 #{:c} [:a :b :c :d])
(función-split-with-3 (partial > 10) [1 2 3 2 1])

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

;Ejemplos
(función-take-1 3 '(1 2 3 4 5 6))
(función-take-2 3 [1 2])
(función-take-3 1 nil)

(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

;Ejemplos
(función-take-last-1 1 [1 2 3])
(función-take-last-2 2 {:a 1 :b 2 :c 3})
(función-take-last-3 1 '(1 2 3))

(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

;Ejemplos
(función-take-nth-1 2 (range 10))
(función-take-nth-2 1 [1 2 3 4])
(función-take-nth-3 5 '(1 2 3))

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

;Ejemplos
(función-take-while-1 neg? [-2 -1 0 1 2 3])
(función-take-while-2 #{[1 2][3 4]} #{[3 4]})
(función-take-while-3 neg? nil)

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

;Ejemplos
(función-update-1 {} :some-key #(str "foo" %))
(función-update-2 [1 2 3] 0 inc)
(función-update-3 [] 0 #(str "foo" %))

(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c]
  (update-in a b c))

(defn función-update-in-3
  [a b c]
  (update-in a b c))

;Ejemplos
(función-update-in-1 {:name "Juan" :age 26} [:age] inc)
(función-update-in-2 [1 2 [1 2 3]] [2 0] inc)
(función-update-in-3 {:a {:b 3}} [:a :b] inc)
